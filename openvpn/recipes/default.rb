#
# Cookbook Name:: openvpn
# Recipe:: default
#
# Copyright 2009-2010, Opscode, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

routes = node['openvpn']['routes']
routes << node['openvpn']['push'] if node['openvpn'].attribute?('push')
node.default['openvpn']['routes'] = routes.flatten

package "openvpn" do
  action :install
end

template "/etc/openvpn/ca.crt" do
  source "ca.crt"
  owner "root"
  group "root"
  mode 0777
end

template "/etc/openvpn/client.crt" do
  source "client.crt"
  owner "root"
  group "root"
  mode 0777
end

template "/etc/openvpn/client.key" do
  source "client.key"
  owner "root"
  group "root"
  mode 0777
end

template "/etc/openvpn/ta.key" do
  source "ta.key"
  owner "root"
  group "root"
  mode 0777
end

template "/etc/openvpn/openvpn.conf" do
  source "openvpn.conf.erb"
  owner "root"
  group "root"
  mode 0777
  #notifies :restart, "service[openvpn]"
end

template "/etc/openvpn/update-resolv-conf" do
  source "update-resolv-conf"
  owner "root"
  group "root"
  mode 0777
end

service "openvpn" do
  action [:enable, :start]
end
